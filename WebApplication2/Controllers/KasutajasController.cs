﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication2;

namespace WebApplication2
{
    partial class Kasutaja
    {
        public string Rollid => string.Join(",", this.KasutajaInRoll.Select(x => x.Roll.Nimetus));
    }
}

namespace WebApplication2.Controllers
{
    public class KasutajasController : Controller
    {
        private RaamatukoguEntities1 db = new RaamatukoguEntities1();

        // GET: Kasutajas
        public ActionResult Index()
        {
            return View(db.Kasutaja.ToList());
        }

        // GET: Kasutajas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kasutaja kasutaja = db.Kasutaja.Find(id);
            if (kasutaja == null)
            {
                return HttpNotFound();
            }
            return View(kasutaja);
        }

        // GET: Kasutajas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Kasutajas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Kood,Email,Nimi")] Kasutaja kasutaja)
        {
            if (ModelState.IsValid)
            {
                db.Kasutaja.Add(kasutaja);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(kasutaja);
        }

        // GET: Kasutajas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kasutaja kasutaja = db.Kasutaja.Find(id);
            if (kasutaja == null)
            {
                return HttpNotFound();
            }
            return View(kasutaja);
        }

        // POST: Kasutajas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Kood,Email,Nimi")] Kasutaja kasutaja)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kasutaja).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kasutaja);
        }

        // GET: Kasutajas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kasutaja kasutaja = db.Kasutaja.Find(id);
            if (kasutaja == null)
            {
                return HttpNotFound();
            }
            return View(kasutaja);
        }

        // POST: Kasutajas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kasutaja kasutaja = db.Kasutaja.Find(id);
            db.Kasutaja.Remove(kasutaja);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
